import sys

def hello(who):
    print("hello", who, "!")

if __name__ == "__main__":
    hello(sys.argv[1])

